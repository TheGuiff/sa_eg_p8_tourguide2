package tourGuide.domain.service;

import java.time.LocalDateTime;
import java.time.ZoneOffset;
import java.util.*;
import java.util.concurrent.*;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import gpsUtil.GpsUtil;
import gpsUtil.location.Location;
import gpsUtil.location.VisitedLocation;
import tourGuide.web.dto.AllCurrentLocationDto;
import tourGuide.web.dto.FiveNearByAttractionDto;
import tourGuide.web.dto.NearByAttractionsDto;
import tourGuide.configuration.InternalTestHelper;
import tourGuide.tracker.Tracker;
import tourGuide.domain.model.User;
import tourGuide.domain.model.UserPreferences;
import tourGuide.domain.model.UserReward;
import tripPricer.Provider;
import tripPricer.TripPricer;

@Service
public class TourGuideService {
	private Logger logger = LoggerFactory.getLogger(TourGuideService.class);
	private final GpsUtil gpsUtil;
	private final RewardsService rewardsService;
	private final TripPricer tripPricer = new TripPricer();
	public final Tracker tracker;
	boolean testMode = true;
	private ExecutorService tourGuideExecutorService = Executors.newFixedThreadPool(1000);
	
	public TourGuideService(GpsUtil gpsUtil, RewardsService rewardsService) {
		this.gpsUtil = gpsUtil;
		this.rewardsService = rewardsService;
		
		if(testMode) {
			logger.info("TestMode enabled");
			logger.debug("Initializing users");
			initializeInternalUsers();
			logger.debug("Finished initializing users");
		}
		tracker = new Tracker(this);
		addShutDownHook();
	}

	/**
	 * Get all the rewards for a user.
	 *
	 * @param user The user object that is being passed in from the controller.
	 * @return A list of UserReward objects.
	 */
	public List<UserReward> getUserRewards(User user) {
		return user.getUserRewards();
	}

	/**
	 * If the user has at least one visited location, return the last one,
	 * otherwise track the user's location and return it."
	 *
	 * The `trackUserLocation` function is asynchronous, so we need to use the `get` method to wait for the result
	 *
	 * @param user The user object that we want to track.
	 * @return A VisitedLocation object.
	 */
	public VisitedLocation getUserLocation(User user) throws ExecutionException, InterruptedException {
		VisitedLocation visitedLocation = (user.getVisitedLocations().size() > 0) ?
			user.getLastVisitedLocation() :
			trackUserLocation(user).get();
		return visitedLocation;
	}

	/**
	 * Return an object User from its name.
	 *
	 * @param userName Name of the object User we want to return
	 * @return A user object.
	 */
	public User getUser(String userName) {
		return internalUserMap.get(userName);
	}

	/**
	 * Return a list of all users in the system.
	 *
	 * @return A list of all the users in the internalUserMap.
	 */
	public List<User> getAllUsers() {
		return internalUserMap.values().stream().collect(Collectors.toList());
	}

	/**
	 * If the user doesn't already exist, add it to the map.
	 *
	 * @param user The user object to be added to the internal map.
	 */
	public void addUser(User user) {
		if(!internalUserMap.containsKey(user.getUserName())) {
			internalUserMap.put(user.getUserName(), user);
		}
	}

	/**
	 * The function gets the user's trip deals based on the user's id, number of
	 * adults, number of children, trip duration, and cumulatative reward points
	 *
	 * @param user The user object that contains the user's preferences and rewards.
	 * @return A list of providers.
	 */
	public List<Provider> getTripDeals(User user) {
		int cumulatativeRewardPoints = user.getUserRewards().stream().mapToInt(i -> i.getRewardPoints()).sum();
		List<Provider> providers = tripPricer.getPrice(tripPricerApiKey, user.getUserId(), user.getUserPreferences().getNumberOfAdults(),
				user.getUserPreferences().getNumberOfChildren(), user.getUserPreferences().getTripDuration(), cumulatativeRewardPoints);
		user.setTripDeals(providers);
		return providers;
	}

	/**
	 * Track Location for user by adding his current location to his list of visited locations and calculating the
	 * rewards of visiting this location
	 *
	 * @param user The user object that we want to track.
	 * @return CompletableFuture<VisitedLocation>
	 */
	public Future<VisitedLocation> trackUserLocation(User user) {
		return CompletableFuture
				.supplyAsync(() -> {
					VisitedLocation visitedLocation = gpsUtil.getUserLocation(user.getUserId());
					user.addToVisitedLocations(visitedLocation);
					rewardsService.calculateRewards(user);
					return visitedLocation;
				}, tourGuideExecutorService);
	}

	/**
	 * Get 5 attractions that are closest to the given location where a user is
	 * Return a FiveNearByAttraction object with :
	 * - The user Id
	 * - The location of the user
	 * - A list of the 5 nearest attractions (for each : name of the attraction, location, distance in miles and reward points)
	 *
	 * @param visitedLocation The location that the user has visited.
	 * @return A FiveNearByAttractionDto object
	 */
	public FiveNearByAttractionDto getFiveNearByAttractions(VisitedLocation visitedLocation, User user) {
		//List des 5 plus proches attractions
		List<NearByAttractionsDto> nearByAttractionsDtos =  gpsUtil.getAttractions()
				.stream()
				.sorted(Comparator.comparing(attraction -> rewardsService.getDistance(attraction, visitedLocation.location)))
				.limit(5)
				.collect(Collectors.toList())
					.stream()
					.map(attraction -> {
						NearByAttractionsDto nearByAttractionsDto = new NearByAttractionsDto(attraction.attractionName,
							new Location(attraction.latitude, attraction.longitude),
							rewardsService.getDistance(attraction, visitedLocation.location),
							rewardsService.getRewardPoints(attraction, user)
							);
						return nearByAttractionsDto;
					})
				.collect(Collectors.toList());
		return new FiveNearByAttractionDto(user.getUserId().toString(), visitedLocation.location, nearByAttractionsDtos);
	}

	/**
	 * Get all the users, get the last visited location of each user and put the user id and location into a map
	 *
	 * @return A map of all the users and their last visited location.
	 */
	public AllCurrentLocationDto getAllUserLocation() {
		AllCurrentLocationDto allCurrentLocationDto = new AllCurrentLocationDto();
		List<User> userList = getAllUsers();
		userList.forEach(user -> {
			allCurrentLocationDto.listCurrentLocation.put(user.getUserId().toString(),
					user.getVisitedLocations().stream()
							.map(visitedLocation -> visitedLocation.location)
							.collect(Collectors.toList()));
		});
		return allCurrentLocationDto;
	}

	/**
	 * Update the user preferences of a user found by his user name
	 *
	 * @param userName Name of the user whom preferences we want to update
	 * @param userPreferences Preferences to update
	 * @return A map of all the users and their last visited location.
	 */
	public UserPreferences updateUserPreferences (String userName, UserPreferences userPreferences) {
		User user = getUser(userName);
		user.setUserPreferences(userPreferences);
		return user.getUserPreferences();
	}
	
	private void addShutDownHook() {
		Runtime.getRuntime().addShutdownHook(new Thread() { 
		      public void run() {
		        tracker.stopTracking();
		      } 
		    }); 
	}

	public void trackUserLocationTerminationAfterShutdown() {
		tourGuideExecutorService.shutdown();
		try {
			if (!tourGuideExecutorService.awaitTermination(5, TimeUnit.MINUTES)) {
				tourGuideExecutorService.shutdownNow();
			}
		} catch (InterruptedException ex) {
			tourGuideExecutorService.shutdownNow();
			Thread.currentThread().interrupt();
		}
		tourGuideExecutorService = Executors.newFixedThreadPool(1000);
	}
	
	/**********************************************************************************
	 * 
	 * Methods Below: For Internal Testing
	 * 
	 **********************************************************************************/
	private static final String tripPricerApiKey = "test-server-api-key";
	// Database connection will be used for external users, but for testing purposes internal users are provided and stored in memory
	private final Map<String, User> internalUserMap = new HashMap<>();
	private void initializeInternalUsers() {
		IntStream.range(0, InternalTestHelper.getInternalUserNumber()).forEach(i -> {
			String userName = "internalUser" + i;
			String phone = "000";
			String email = userName + "@tourGuide.com";
			User user = new User(UUID.randomUUID(), userName, phone, email);
			generateUserLocationHistory(user);

			internalUserMap.put(userName,user);
		});
		logger.debug("Created " + InternalTestHelper.getInternalUserNumber() + " internal test users.");
	}
	
	private void generateUserLocationHistory(User user) {
		IntStream.range(0, 3).forEach(i-> {
			user.addToVisitedLocations(new VisitedLocation(user.getUserId(), new Location(generateRandomLatitude(), generateRandomLongitude()), getRandomTime()));
		});
	}
	
	private double generateRandomLongitude() {
		double leftLimit = -180;
	    double rightLimit = 180;
	    return leftLimit + new Random().nextDouble() * (rightLimit - leftLimit);
	}
	
	private double generateRandomLatitude() {
		double leftLimit = -85.05112878;
	    double rightLimit = 85.05112878;
	    return leftLimit + new Random().nextDouble() * (rightLimit - leftLimit);
	}
	
	private Date getRandomTime() {
		LocalDateTime localDateTime = LocalDateTime.now().minusDays(new Random().nextInt(30));
	    return Date.from(localDateTime.toInstant(ZoneOffset.UTC));
	}
	
}
