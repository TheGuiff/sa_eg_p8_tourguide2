package tourGuide.web.dto;

import gpsUtil.location.Location;

import java.util.List;

public class FiveNearByAttractionDto {

    private final String userId;
    private final Location userLocation;
    private final List<NearByAttractionsDto> nearByAttractionsDtos;

    public FiveNearByAttractionDto(String userIdIn,
                                   Location userLocationIn,
                                   List<NearByAttractionsDto> nearByAttractionsDtosIn) {
        this.userId = userIdIn;
        this.userLocation = userLocationIn;
        this.nearByAttractionsDtos = nearByAttractionsDtosIn;
    }

    public List<NearByAttractionsDto> getNearByAttractionsDtos() {
        return nearByAttractionsDtos;
    }
}
