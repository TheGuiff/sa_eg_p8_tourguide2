package tourGuide.web.dto;

import gpsUtil.location.Location;

public class NearByAttractionsDto {

    private final String nameOfTouristAttraction;
    private final Location attractionLocation;
    private final Double distanceInMiles;
    private final int rewardPoints;

    public NearByAttractionsDto (String nameOfTouristAttractionIn, Location attractionLocationIn,
                                Double distanceInMilesIn, int rewardPointsIn) {
        this.nameOfTouristAttraction = nameOfTouristAttractionIn;
        this.attractionLocation = attractionLocationIn;
        this.distanceInMiles = distanceInMilesIn;
        this.rewardPoints = rewardPointsIn;
    }

    public String getNameOfTouristAttraction() {return nameOfTouristAttraction;}

    public Location getAttractionLocation() {return attractionLocation;}

    public Double getDistanceInMiles() {return distanceInMiles;}

    public int getRewardPoints() {return rewardPoints;}

}
