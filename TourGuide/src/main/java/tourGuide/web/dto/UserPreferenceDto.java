package tourGuide.web.dto;

import tourGuide.domain.model.UserPreferences;

public class UserPreferenceDto {

    public String attractionProximity;
    public String lowerPricePoint;
    public String highPricePoint;
    public String tripDuration;
    public String ticketQuantity;
    public String numberOfAdults;
    public String numberOfChildren;

    public UserPreferenceDto(){

    }

    public UserPreferenceDto (UserPreferences userPreferences) {
        this.attractionProximity = String.valueOf(userPreferences.getAttractionProximity());
        this.lowerPricePoint = String.valueOf(userPreferences.getLowerPricePoint());
        this.highPricePoint = String.valueOf(userPreferences.getHighPricePoint());
        this.tripDuration = String.valueOf(userPreferences.getTripDuration());
        this.ticketQuantity = String.valueOf(userPreferences.getTicketQuantity());
        this.numberOfAdults = String.valueOf(userPreferences.getNumberOfAdults());
        this.numberOfChildren = String.valueOf(userPreferences.getNumberOfChildren());
    }


}
