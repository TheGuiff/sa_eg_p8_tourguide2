package tourGuide.web.dto;

import gpsUtil.location.Location;

import java.util.HashMap;
import java.util.List;

public class AllCurrentLocationDto {

    public HashMap<String, List<Location>> listCurrentLocation;

    public AllCurrentLocationDto() {
        listCurrentLocation = new HashMap<>();
    }

}
