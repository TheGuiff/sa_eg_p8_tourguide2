package tourGuide.web;

import java.util.List;
import java.util.concurrent.ExecutionException;

import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import com.jsoniter.output.JsonStream;

import gpsUtil.location.VisitedLocation;
import tourGuide.web.dto.UserPreferenceDto;
import tourGuide.domain.service.TourGuideService;
import tourGuide.domain.model.User;
import tourGuide.domain.model.UserPreferences;
import tripPricer.Provider;

@RestController
public class TourGuideController {

	@Autowired
	TourGuideService tourGuideService;

    /**
     * The function returns a string,
     * It's mapped to the root URL
     *
     * @return A string
     */
    @ApiOperation(value="Greetings from TouGuide")
    @GetMapping("/")
    public String index() {
        return "Greetings from TourGuide!";
    }

    /**
     * The function returns the location of the user with the given userName
     *
     * @param userName The name of the user whose location we want to get.
     * @return The location of the user in JSON format
     */
    @ApiOperation(value="Location of the user with the given userName")
    @GetMapping("/getLocation")
    public String getLocation(@RequestParam String userName) throws ExecutionException, InterruptedException {
    	VisitedLocation visitedLocation = tourGuideService.getUserLocation(getUser(userName));
		return JsonStream.serialize(visitedLocation.location);
    }

    /**
     * This function takes in a userName as a parameter and returns the list of the five nearest attractions in JSON format
     *
     * @param userName The user name of the user who is requesting the nearby attractions.
     * @return A JSON list of five nearby attractions.
     */
    @ApiOperation(value="List of the five nearest attractions")
    @GetMapping("/getNearbyAttractions")
    public String getNearbyAttractions(@RequestParam String userName) throws ExecutionException, InterruptedException {
    	VisitedLocation visitedLocation = tourGuideService.getUserLocation(getUser(userName));
    	return JsonStream.serialize(tourGuideService.getFiveNearByAttractions(visitedLocation, getUser(userName)));
    }

    /**
     * This function returns the rewards that a user has earned
     *
     * @param userName The user's name
     * @return A JSON string of the user's rewards.
     */
    @ApiOperation(value="Rewards that a user has earned")
    @GetMapping("/getRewards")
    public String getRewards(@RequestParam String userName) {
    	return JsonStream.serialize(tourGuideService.getUserRewards(getUser(userName)));
    }

    /**
     * This function returns a JSON string of all the current locations of all the users
     *
     * @return A list of all the current locations of the users
     */
    @ApiOperation(value="All the current locations of all the users")
    @GetMapping("/getAllCurrentLocations")
    public String getAllCurrentLocations() {
    	return JsonStream.serialize(tourGuideService.getAllUserLocation());
    }

    /**
     * This function returns a list of providers that have deals for the user
     *
     * @param userName The user name of the user who is requesting the trip deals.
     * @return A list of providers
     */
    @ApiOperation(value="List of providers that have deals for the user")
    @GetMapping("/getTripDeals")
    public String getTripDeals(@RequestParam String userName) {
    	List<Provider> providers = tourGuideService.getTripDeals(getUser(userName));
    	return JsonStream.serialize(providers);
    }

    /**
     * This function update the preferences of a user designated by his user name
     *
     * @param userName Name of the user whom preferencees we want to update
     * @param userPreferenceDtoIn Flat Preferences to update
     * @return The updated preferences
     */
    @ApiOperation(value="Update user preferences")
    @PutMapping("/updateUserPreferences")
    public String updateUserPreferences(@RequestParam String userName,
                                        @RequestBody UserPreferenceDto userPreferenceDtoIn) {
        UserPreferenceDto userPreferenceDtoOut = new UserPreferenceDto(tourGuideService.updateUserPreferences(userName, new UserPreferences(userPreferenceDtoIn)));
        return JsonStream.serialize(userPreferenceDtoOut);
    }

    private User getUser(String userName) {
    	return tourGuideService.getUser(userName);
    }
   

}